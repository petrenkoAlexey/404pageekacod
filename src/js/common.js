$(document).ready(function () {

    const login = document.querySelector('.header_logIn');
    const loginClose = document.querySelector('.login-cross');

    $("body").on("click", function (e) {

        const target = e.target;
        const its_menu = target == login || login.contains(target);
        const its_close = target === loginClose

        if (its_menu && !its_close) {
            login.classList.add("active")
            $(this).addClass("overlay")
        } else {
            login.classList.remove("active")
            $(this).removeClass("overlay")
        }
    })

});
